﻿namespace AR_One
{
    using System;
    using System.Collections.Generic;

    public struct Arone
    {
        public long frequency;

        public int Mode;

        public Int16 Bandwidth;

        public int HighPassFilter;

        public int LowPassFilter;

        public int AutomaticGainControl;

        public int Attenuator;

        public int Amplifier;

        public int NoiseSquelchTreshold;

        public int LevelSquelchTreshold;

        public int AGC;

        public int AFGain;

        public int ManualGain;

        public int RFGain;

        public int IFGain;

        public int SquelchSelect;

        public int SignalLevel;

        public int AutoSignalLevel;

        public int AutoBackLit;

        public int BackLit_OnOff;

        public int BackLitDimmer;

        public int LCDContrast;

        public int BeepLevel;

        public int SpeakerSelect;

        public int ExternalSpeacker;

        public static string DelayTime;

        public static double FreeScan;

        public int SignalMeterDisplay;

        public int DuplexMode;

        public int[] DuplexFrequency;

        public int SignalLevelUnit_dBmV;

        public int SignalLevelUnit_dBm;

        public int[] PassFrequencyList;

        public int SelectMemory_OnOff;

        public int SelectMemoryList;

        public int DEemphasis;

        public int BFOFreq;

        public bool PortIsOpen;

        public static string MBank;

        public static string MChannel;

        public static string MemoryData;

        public static string ModeScan;

        public static List<string> memory;

        public static List<List<string>> dataChannel = new List<List<string>>();

        public static List<string> memTwo = new List<string>();

        public static List<string> memthree = new List<string>();
    }

}