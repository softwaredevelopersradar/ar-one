﻿using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace AR_One
{
    using System;
    using System.IO.Ports;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    using NLog;

    public class ArOneAsync
    {
        public Arone ArOne = new Arone();

        public SerialPort Port;


        public ArOneAsync()
        {
            this.Logger = LogManager.GetCurrentClassLogger();
        }

        public event EventHandler OnConnectPort;

        public event EventHandler OnDisconnectPort;

        public event EventHandler OnReadByte;

        public event EventHandler OnWriteByte;

        public ILogger Logger { get; set; }

        public Task OpenPort(string portName, int baudRate)
        {
            if (this.Port == null)
            {
                this.Logger.Trace("Created new SerialPort object");
                this.Port = new SerialPort();
            }

            if (this.Port.IsOpen)
            {
                this.Logger.Trace("Port already opened. Close old one.");
                this.ClosePort();
                this.ArOne.PortIsOpen = this.Port.IsOpen;
            }

            try
            {
                this.Port.PortName = portName;
                this.Port.BaudRate = baudRate;
                this.Port.Parity = Parity.None;
                this.Port.DataBits = 8;
                this.Port.StopBits = StopBits.Two;
                this.Port.RtsEnable = true;
                this.Port.DtrEnable = true;
                this.Port.ReceivedBytesThreshold = 1000;
                this.Port.Open();

                this.ArOne.PortIsOpen = this.Port.IsOpen;
                if (this.ArOne.PortIsOpen)
                {
                    this.Logger.Debug($"Port: {portName} opened!");
                }
                this.OnConnectPort?.Invoke(this, EventArgs.Empty);
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error during openning serial port {ex.Message}");
                this.OnDisconnectPort?.Invoke(this, EventArgs.Empty);
                return Task.FromException(ex);
            }
        }

        public Task ClosePort()
        {
            try
            {
                this.Port?.DiscardInBuffer();
                this.Port?.DiscardOutBuffer();
                this.Port?.BaseStream.Close();
                this.Port?.Close();
                this.Logger.Trace("Serial port closed");
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while closing com port {ex.Message}");
                return Task.FromException(ex);
            }
        }

        public async Task<bool> TurnOn(CancellationToken token = default)
        {
            try
            {
                var message = "X\x0D\x0A";
                await this.SendStream(message, token).ConfigureAwait(false);

                this.Logger.Trace("Turn on command sent");
                // Thread.Sleep(40);
                return true;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while sending turn on command {ex.Message}");
                return false;
            }
        }

        public async Task<bool> TurnOff(CancellationToken token = default)
        {
            try
            {
                string message = "QP\x0D\x0A";
                await this.SendStream(message, token).ConfigureAwait(false);
                this.Logger.Trace("Turn off command sent");

                // Thread.Sleep(40);
                return true;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while sending turn off command {ex.Message}");
                return false;
            }
        }

        public Task<bool> FrequencySet(double freqMhz, CancellationToken token = default)
        {
            return this.FrequencySet(Convert.ToInt64(freqMhz * 1000000), token);
        }

        public async Task<long> FrequencyGet(CancellationToken token = default)
        {
            try
            {
                var message = "RF\x0D\x0A";
                await this.SendStream(message, token).ConfigureAwait(false);
                this.Logger.Trace($"Frequency get command sent");
                await Task.Delay(100, token);
                var result = await this.ReadCommand(CommandType.RF, token).ConfigureAwait(false);
                // Thread.Sleep(20);
                return result.frequency;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while sending frequency get command {ex.Message}");
                return -1;
            }
        }

        public async Task<bool> FrequencySet(long freqHz, CancellationToken token = default) //Hz
        {
            try
            {
                if ((freqHz > 3300000000) || (freqHz < 10000))
                {
                    this.Logger.Error($"Frequecy value out of range [10000, 3300000000]. Current value {freqHz}!");
                    return false;
                }
                var freqHzString = freqHz.ToString();
                freqHzString = freqHzString.PadLeft(10, '0');
                string message = "RF" + freqHzString + "\x0D\x0A";
                await this.SendStream(message, token).ConfigureAwait(false);
                this.Logger.Trace($"Frequency set command sent");
                return true;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while sending frequency set command {ex.Message}");
                return false;
            }
        }

        public async Task<bool> ModeSet(int mode, CancellationToken token = default)
        {
            try
            {
                if ((mode > 6) || (mode < 0))
                {
                    this.Logger.Error($"Mode value out of range (0, 6). Current value {mode}!");
                    return false;
                }
                string message = "MD" + mode.ToString() + "\x0D\x0A";
                await this.SendStream(message, token).ConfigureAwait(false);
                this.Logger.Trace($"Mode set command sent");

                return true;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while sending mode set command {ex.Message}");
                return false;
            }
        }

        public async Task<int> ModeGet(CancellationToken token = default)
        {
            try
            {
                await this.SendStream("MD\x0D\x0A", token).ConfigureAwait(false);
                this.Logger.Trace($"Mode get command sent");
                var result = await this.ReadCommand(CommandType.MD).ConfigureAwait(false);
                return result.Mode;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while sending mode get command {ex.Message}");
                return -1;
            }
        }

        public async Task<bool> BandWidthSet(int bw, CancellationToken token = default)
        {
            try
            {
                if ((bw > 8) || (bw < 0)) return false;
                string message = "BW" + bw.ToString() + "\x0D\x0A";
                await this.SendStream(message, token).ConfigureAwait(false);
                this.Logger.Trace($"Bandwidth set command sent");

                return true;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while sending bandwidth set command {ex.Message}");
                return false;
            }
        }

        public async Task<short> BandwidthGet(CancellationToken token = default)
        {
            try
            {
                await this.SendStream("BW\x0D\x0A", token).ConfigureAwait(false);
                this.Logger.Trace($"Bandwidth get command sent");
                var result = await this.ReadCommand(CommandType.BW).ConfigureAwait(false);
                return result.Bandwidth;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while sending bandwidth get command {ex.Message}");

                return -1;
            }
        }

        private async Task SendStream(string message, CancellationToken token = default)
        {
            var ascii = Encoding.ASCII.GetBytes(message);
            await this.Port.BaseStream.WriteAsync(ascii, 0, ascii.Length, token).ConfigureAwait(false);
            await Task.Delay(50, token);

            // it's important to flush stream cause on the next stream reading we will read the last wrote message. Trouble on LINUX!
            /*
            await this.Port.BaseStream.FlushAsync(token).ConfigureAwait(false);
            */
            this.Port.DiscardInBuffer();
            this.Port.DiscardOutBuffer();
        }

        private async Task<Arone> ReadCommand(CommandType requestedCommand, CancellationToken token = default)
        {
            var aroneAnswer = new Arone();


                switch (requestedCommand)
                {
                    case CommandType.RF:
                    {
                        var data = await this.ReadStream(15, token).ConfigureAwait(false);
                        string convString = String.Empty;
                        
                        convString = Regex.Replace(Encoding.ASCII.GetString(data, 3, data.Length - 3), @"[^\d]",
                            "");
                        long.TryParse(convString, out aroneAnswer.frequency);
                        return aroneAnswer;
                    }
                    case CommandType.MD:
                        {
                            var data = await this.ReadStream(5).ConfigureAwait(false);
                            var convString = Encoding.ASCII.GetString(data, 2, 1);
                            int.TryParse(convString, out aroneAnswer.Mode);
                            return aroneAnswer; 
                        }
                    case CommandType.BW:
                        {
                            var data = await this.ReadStream(5).ConfigureAwait(false);
                            var convString = Encoding.ASCII.GetString(data, 2, 1);
                            short.TryParse(convString, out aroneAnswer.Bandwidth);
                            return aroneAnswer;
                    }
                    case CommandType.HP:
                    case CommandType.LP:
                    case CommandType.AC:
                    case CommandType.EN:
                    case CommandType.AT:
                    case CommandType.AM:
                    case CommandType.BF:
                    case CommandType.RQ:
                    case CommandType.DB:
                    case CommandType.AG:
                    case CommandType.MG:
                    case CommandType.RG:
                    case CommandType.IG:
                    case CommandType.SQ:
                    case CommandType.LS:
                    case CommandType.NS:
                    case CommandType.LC:
                    case CommandType.LA:
                    case CommandType.BL:
                    case CommandType.LD:
                    case CommandType.LV:
                    case CommandType.BV:
                    case CommandType.SO:
                    case CommandType.PO:
                    case CommandType.DD:
                    case CommandType.SP:
                    case CommandType.SF:
                    case CommandType.OF:
                    case CommandType.OL:
                    case CommandType.LU:
                    case CommandType.LB:
                    case CommandType.GA:
                    case CommandType.GR:
                    case CommandType.MR:
                    case CommandType.MX:
                    case CommandType.MS:
                    default:
                        throw new ArgumentOutOfRangeException(nameof(requestedCommand), requestedCommand, null);
                }
        }

        private async Task<byte[]> ReadStream(int bytesToRead, CancellationToken token = default)
        {
            var fullAnswer = new byte[bytesToRead];
            var temp = new byte[bytesToRead];
            var leftToRead = bytesToRead;
            this.Port.BaseStream.ReadTimeout = 500;
            try
            {
                while (leftToRead > 0 && !token.IsCancellationRequested)
                {
                    var readBytes = await this.Port.BaseStream.ReadAsync(temp, 0, bytesToRead, token).ConfigureAwait(false);
                    Array.Copy(temp, 0, fullAnswer,  bytesToRead - leftToRead, readBytes);
                    leftToRead -= readBytes;
                }
            }
            catch (Exception e)
            {
                return new byte[] {0};
            }
           

            return fullAnswer;
        }
    }
}