﻿namespace AR_One
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.IO.Ports;

    public class AR_One
    {
        public Arone ArOne = new Arone();
        public static SerialPort port;
        private Thread thrRead;
        String strRead = "";

        public delegate void ByteEventHandler();
        public delegate void ByteEventHandler2(string cmd, int data);
        public delegate void ConnectEventHandler();
        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler2 OnDecodedByte;
        public event ByteEventHandler OnWriteByte;
        public event ConnectEventHandler OnConnectPort;
        public event ConnectEventHandler OnDisconnectPort;
        public event ByteEventHandler OnDecodedBW;
        public event ByteEventHandler OnDecodedAGC;
        public event ByteEventHandler OnDecodedAFGain;
        public event ByteEventHandler OnDecodedAMpl;
        public event ByteEventHandler OnDecodedATT;
        public event ByteEventHandler OnDecodedAutoSignalLevel;
        public event ByteEventHandler OnDecodedError;
        public event ByteEventHandler OnDecodedHPF;
        public event ByteEventHandler OnDecodedLPF;
        public event ByteEventHandler OnDecodedIFGain;
        public event ByteEventHandler OnDecodedRFGain;
        public event ByteEventHandler OnDecodedLevelSquelchTreshold;
        public event ByteEventHandler OnDecodedManualGain;
        public event ByteEventHandler OnDecodedMode;
        public event ByteEventHandler OnDecodedNoiseSquelchTreshold;
        public event ByteEventHandler OnDecodedFrq;
        public event ByteEventHandler OnDecodedSignalLevelUnit_dBm;
        public event ByteEventHandler OnDecodedSignalLevelUnit_dBmV;
        public event ByteEventHandler OnDecodedSquelchSelect;
        public event ByteEventHandler OnDecodedSignalLevel;
        public static event ByteEventHandler OnDecodedMemoryChannelDataRead;
        public static event ByteEventHandler OnDecodedScanMode;
        public static event ByteEventHandler OnFreeScan;
        public static event ByteEventHandler OnDelayScan;
        public event ByteEventHandler OnPortIsOpen;

        public bool SendToArone(string message)
        {
            try
            {
                message += "\x0D\x0A";
                port.WriteLine(message);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool TurnON()
        {
            try
            {
                string message = "X\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(40);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool TurnOFF()
        {
            try
            {
                string message = "QP\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(40);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool FrequencySet(double FrqMhz)
        {
            try
            {
                if ((FrqMhz < 0.09) || (FrqMhz > 3300)) return false;
                long FrqHz = (Int64)(FrqMhz * 1000000);
                string frq = FrqHz.ToString().PadLeft(10, '0');
                string message = "RF" + frq + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool FrequencyGet()
        {
            try
            {
                port?.WriteLine("RF\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool FrequencySet(string freq_MHz)//MHz
        {
            try
            {
                long frql;
                double frqd;
                if (!double.TryParse(freq_MHz, out frqd)) { frqd = 0; return false; }
                frqd = 1000000 * frqd;
                if ((frqd > 3300000000) || (frqd < 10000)) { return false; }
                frql = (long)frqd;
                freq_MHz = frql.ToString();
                freq_MHz = freq_MHz.PadLeft(10, '0');
                string message = "RF" + freq_MHz + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(40);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool ModeSet(int Mode)
        {
            try
            {
                if ((Mode > 6) || (Mode < 0)) return false;
                string message = "MD" + Mode.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool ModeGet()
        {
            try
            {
                port?.WriteLine("MD\x0D\x0A");
                Thread.Sleep(40);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool BandWidthSet(int BW)
        {
            try
            {
                if ((BW > 8) || (BW < 0)) return false;
                string message = "BW" + BW.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool BandwidthGet()
        {
            try
            {
                port?.WriteLine("BW\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool HighPassFiltrSet(string HPF)
        {
            try
            {
                string message;

                //if ((HPF.ToString() != "0") || (HPF.ToString() != "1") || (HPF.ToString() != "2")
                //    || (HPF.ToString() != "3") || (HPF.ToString() != "4")) { return false; }

                if ((HPF.ToString() == "0") || (HPF.ToString() == "1") || (HPF.ToString() == "2")
                    || (HPF.ToString() == "3") || (HPF.ToString() == "4"))
                {
                    if (HPF.ToString() != "4") { message = "HP" + HPF.ToString() + "\x0D\x0A"; }
                    else { message = "HP" + "F" + "\x0D\x0A"; }
                    port?.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                else { return false; }
            }
            catch (Exception ex) { return false; }
        }
        public bool HighPassFiltrGet()
        {
            try
            {
                port?.WriteLine("HP\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool LowPassFiltrSet(string LPF)
        {
            try
            {
                //if ((LPF.ToString() != "0") || (LPF.ToString() != "1") || (LPF.ToString() != "2")
                //    || (LPF.ToString() != "3") || (LPF.ToString() != "4")) { return false; }
                string message;// = "LP" + LPF.ToString() + "\x0D\x0A";
                if ((LPF.ToString() == "0") || (LPF.ToString() == "1") || (LPF.ToString() == "2")
                    || (LPF.ToString() == "3") || (LPF.ToString() == "4"))
                {
                    if (LPF.ToString() != "4") { message = "LP" + LPF.ToString() + "\x0D\x0A"; }
                    else { message = "LP" + "F" + "\x0D\x0A"; }

                    port?.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                else { return false; }
            }
            catch (Exception ex) { return false; }
        }
        public bool LowPassFiltrGet()
        {
            try
            {
                port?.WriteLine("LP\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AutomaticGainControlSet(int AGC)
        {
            try
            {
                if ((AGC > 3) || (AGC < 0)) return false;
                string message = "AC" + AGC.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AutomaticGainControlGet()
        {
            try
            {
                port?.WriteLine("AC\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DE_EmphasisSet(int DE_E)
        {
            try
            {
                if ((DE_E > 5) || (DE_E < 0)) return false;
                string message = "EN" + DE_E.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DE_EmphasisGet()
        {
            try
            {
                port?.WriteLine("EN\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AttenuatorSet(int Att)
        {
            try
            {
                if ((Att > 3) || (Att < 0)) return false;
                string message = "AT" + Att.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AttenuatorGet()
        {
            try
            {
                port?.WriteLine("AT\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AmplifierSet(int Ampl)
        {
            try
            {
                if ((Ampl > 2) || (Ampl < 0)) return false;
                string message = "AM" + Ampl.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AmplifierGet()
        {
            try
            {
                port?.WriteLine("AM\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool NoiseSQSet(int NSQ)
        {
            try
            {
                if ((NSQ > 255) || (NSQ < 0)) return false;
                string message = "RQ " + NSQ.ToString().PadLeft(3, '0') + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool NoiseSQGet()
        {
            try
            {
                port?.WriteLine("RQ\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool LevelSQSet(double LSQ)
        {
            try
            {
                if ((LSQ > 255) || (LSQ < 0)) return false;
                string message = "DB " + LSQ.ToString().PadLeft(3, '0') + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool LevelSQGet()
        {
            try
            {
                port?.WriteLine("DB\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AFGainSet(double AG)
        {
            try
            {
                if ((AG > 255) || (AG < 0)) return false;
                string message = "AG " + AG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                port?.WriteLine(message);
                //Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AFGainGet()
        {
            try
            {
                port?.WriteLine("AG\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool ManualGainSet(int MG, int AGC)//10.7MHz AGC
        {
            try
            {
                if ((MG > 255) || (MG < 0) || (AGC != 0)) return false;
                string message = "MG" + MG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool ManualGainGet()
        {
            try
            {
                port?.WriteLine("MG\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool RFGainSet(int RFG)
        {
            try
            {
                if ((RFG > 255) || (RFG < 0)) return false;
                string message = "RG " + RFG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool RFGainGet()
        {
            try
            {
                port?.WriteLine("RG\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool IFGainSet(int IFG)
        {
            try
            {
                if ((IFG > 255) || (IFG < 0)) return false;
                string message = "IG " + IFG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool IFGainGet()
        {
            try
            {
                port?.WriteLine("IG\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool SquelchSelectSet(int SS)
        {
            try
            {
                //if ((SS !=0) || (SS !=1)) return true;
                //string message = "SQ" + SS.ToString().PadLeft(3, '0') + "\x0D\x0A";
                string message = "SQ" + SS.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool SquelchSelectGet()
        {
            try
            {
                port?.WriteLine("SQ\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool SignalLevelGet()
        {
            try
            {
                if (port != null)
                {
                    if (port.IsOpen)
                    {
                        port.WriteLine("LM\x0D\x0A");
                        Thread.Sleep(20);
                        return true;
                    }
                }
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AutoSignalLevelSet(int ASL)
        {
            try
            {
                if ((ASL > 1) || (ASL < 0)) return false;
                string message = "LC" + ASL.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AutoSignalLevelGet()
        {
            try
            {
                port?.WriteLine("LC\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AutoBackLitSet(int ABS)
        {
            try
            {
                if ((ABS > 2) || (ABS < 0)) return false;
                string message = "LA" + ABS.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AutoBackLitGet()
        {
            try
            {
                port?.WriteLine("LA\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool BackLitSet(int ABS, int AutoBackLit)
        {
            try
            {
                if ((ABS > 2) || (ABS < 0) || (AutoBackLit != 2)) return false;
                string message = "BL" + ABS.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool BackLitGet()
        {
            try
            {
                port?.WriteLine("BL\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool BackLitDimmerSet(int BD)
        {
            try
            {
                if ((BD > 1) || (BD < 0)) return false;
                string message = "LD" + BD.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool BackLitDimmerGet()
        {
            try
            {
                port?.WriteLine("LD\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool LCDContrastSet(int LCDC)
        {
            try
            {
                if ((LCDC > 31) || (LCDC < 0)) return false;
                string message = "LV" + LCDC.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool LCDContrastGet()
        {
            try
            {
                port?.WriteLine("LV\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool BeepLevelSet(int BLev)
        {
            try
            {
                if ((BLev > 9) || (BLev < 0)) return false;
                string message = "BV" + BLev.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool BeepLevelGet()
        {
            try
            {
                port?.WriteLine("BV\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool SpeackerSelectSet(int Speacker)
        {
            try
            {
                if ((Speacker > 3) || (Speacker < 0)) return false;
                string message = "SO" + Speacker.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool SpeackerSelectGet()
        {
            try
            {
                port?.WriteLine("SO\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool ExternalSpeackerSet(int ExtSpk)
        {
            try
            {
                if ((ExtSpk > 1) || (ExtSpk < 0)) return false;
                string message = "PO" + ExtSpk.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool ExternalSpeackerGet()
        {
            try
            {
                port?.WriteLine("PO\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool DelayTime_ScanDelaySet(string DT)
        {
            try
            {
                //if ((DT > 9.9) || (DT < 0)) return false;
                //string DTStr = DT.ToString();
                //if (DTStr.Length >= 2) DTStr = DTStr.Substring(0, 3); else DTStr = DTStr + ".0";
                string message = "DD" + DT + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool DelayTime_ScanDelayGet()
        {
            try
            {
                port?.WriteLine("DD\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool ScanRestartSet(int restart)
        {
            try
            {
                if ((restart < 0) || (restart > 1)) return false;
                string tRes = restart.ToString();
                string message = "SG" + tRes + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool ScanRestartGet()
        {
            try
            {
                port?.WriteLine("SG\"\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch { return false; }
        }
        public static bool FreeScanSet(double FSTime)
        {
            try
            {
                if ((FSTime > 9.9) || (FSTime < 0)) return false;
                string DTStr = FSTime.ToString();
                if (DTStr.Length >= 2) DTStr = DTStr.Substring(0, 3); else DTStr = DTStr + ".0";
                string message = "SP" + DTStr + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool FreeScanGet()
        {
            try
            {
                port?.WriteLine("SP\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool ModeScanSet(int bank)
        {
            try
            {
                string bankSet = bank.ToString();
                string message = "MS" + bankSet + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch { return false; }
        }
        public static bool RXModeGet()
        {
            try
            {
                port?.WriteLine("RX \x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch { return false; }
        }
        public static bool ModeVFOSet(string Vfo)
        {
            try
            {
                string message = "V" + Vfo + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch { return false; }
        }
        public bool SignalMeterDisplaySet(int SMDMode)
        {
            try
            {
                if ((SMDMode > 2) || (SMDMode < 0)) return false;
                string message = "SF" + SMDMode.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool SignalMeterDisplayGet()
        {
            try
            {
                port?.WriteLine("SF\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DuplexModeSet(int DM, string OfsetDirectionPlusOrMinus)
        {
            try
            {
                if ((OfsetDirectionPlusOrMinus != "+") && (OfsetDirectionPlusOrMinus != "-")) return false;
                if ((DM > 47) || (DM < 0)) return false;
                string message = "OF" + DM.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DuplexModeGet()
        {
            try
            {
                port?.WriteLine("OF\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DuplexFrequencySet(int num, int DplFreqHz)
        {
            try
            {
                if ((num > 19) || (num < 1) || (DplFreqHz < 10001) || (DplFreqHz > 999999999)) return false;
                string DplFrqStr = DplFreqHz.ToString().PadLeft(10, '0');
                if (DplFrqStr.Substring(8, 2) != "00") return false;
                string message = "OL" + num.ToString().PadLeft('0') + " " + DplFreqHz.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DuplexFrequencyGet()
        {
            try
            {
                if ((port == null) || (port.IsOpen != true)) return false;
                port?.WriteLine("OL\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool SignalLevelUnit_dBmV_Get()
        {
            try
            {
                port.WriteLine("LU\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool SignalLevelUnit_dBm_Get()
        {
            try
            {
                if ((port == null) || (port.IsOpen != true)) return false;
                port?.WriteLine("LB\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool SearchDataSettingSet_nowork(int BankNum)
        {
            return false;
            //try
            //{
            //    if (( > 2) || ( < 0)) return false;
            //    string message = "" + .ToString() + "\x0D\x0A";
            //    port.WriteLine(message);
            //    Thread.Sleep(20);
            //    return true;
            //}
            //catch (Exception ex) { return false; }
        }
        public bool SearchDataListGet()
        {
            try
            {
                port?.WriteLine("SR\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool PassFrequencySet(int Frq)
        {
            try
            {
                if ((Frq > 3300000000) || (Frq < 30000)) return false;
                string message = "PW" + Frq.ToString().PadLeft(10, '0') + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool PassFrequencyListGet(int BankNum)
        {
            try
            {
                if ((BankNum > 40) || (BankNum < 1)) return false;
                port?.WriteLine("PR" + BankNum.ToString().PadLeft(2, '0') + "\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DeletePassFrequencySet(int BankNum, int PassChannel)
        {
            try
            {
                if ((BankNum > 40) || (BankNum < 1) || (PassChannel > 49) || (PassChannel < 0)) return false;
                string message = "PD" + BankNum.ToString().PadLeft(2, '0') + PassChannel.ToString().PadLeft(2, '0') + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DeletePassFrequencyAllChannelsInBankSet(int BankNum)
        {
            try
            {
                if ((BankNum > 40) || (BankNum < 1)) return false;
                string message = "PD" + BankNum.ToString().PadLeft(2, '0') + "%%\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DeleteSearchDataWithPassFrequencySet(int BankNum)
        {
            try
            {
                if ((BankNum > 40) || (BankNum < 1)) return false;
                string message = "QS" + BankNum.ToString().PadLeft(2, '0') + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool DeleteSearchDataWithPassFrequencyAllBanksSet()
        {
            try
            {
                string message = "QS%%\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool TransferCurrentSearchDataToVFOSet(int NumVFO)
        {
            try
            {
                if ((NumVFO > 9) || (NumVFO < 0)) return false;
                string message = "SV" + NumVFO.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool MemoryDataSettingSet(string adress)
        {
            try
            {
                //if ((BankNum > 9) || (BankNum < 0)||(MemoryChannel>99)||(MemoryChannel<0)) return false;
                string message = "MX" + adress + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public long FrqStrMhzToLongHz(string FreqMHz)
        {
            long frql;
            double frqd;
            if (!double.TryParse(FreqMHz, out frqd)) { return 0; }//MessageBox.Show("Введите частоту в диапазоне \nот 0,01 до 3 300  МГц"); }
            frqd = 1000000 * frqd;
            frql = (long)frqd;
            return frql;
        }
        public static bool MemoryChannelDataReadGet()
        {
            try
            {
                string bNumber = Arone.MBank;
                port?.WriteLine("MA" + bNumber + "\x0D\x0A");
                Thread.Sleep(100);
                for (int i = 0; i < 9; i++)
                {
                    Thread.Sleep(100);
                    port?.WriteLine("MA" + "\x0D\x0A");
                    Thread.Sleep(100);
                }
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public static bool SelectMemoryList()
        {
            try
            {
                string bNumber = Arone.MBank;
                string chNumber = Arone.MChannel;
                port.WriteLine("GR" + "\x0D\x0A");
                //port.WriteLine("MA" + bNumber + "\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch { return false; }
        }
        public static bool MemoryMode()
        {
            try
            {
                port.WriteLine("MR101" + "\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch { return false; }
        }
        public static bool DeleteMemory(int delBank)
        {
            try
            {
                port?.WriteLine("MQ" + delBank.ToString() + "\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch { return false; }
        }
        public static bool SelectMemoryOn()
        {
            try
            {
                string on = "1";
                port.WriteLine("GA" + on + "\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch { return false; }
        }
        public void OpenPort(string portName, int baudRate)
        {
            if (port == null)
                port = new SerialPort();
            if (port.IsOpen)
            {
                ClosePort();
                ArOne.PortIsOpen = port.IsOpen;
            }

            try
            {
                port.PortName = portName;
                port.BaudRate = baudRate;
                port.Parity = Parity.None;
                port.DataBits = 8;
                port.StopBits = StopBits.Two;
                port.RtsEnable = true;
                port.DtrEnable = true;
                port.ReceivedBytesThreshold = 1000;
                port.Open();
                ArOne.PortIsOpen = port.IsOpen;

                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadExistingComPort));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (Exception ex) { }
                ConnectPort();
            }
            catch (Exception ex)
            {
                DisconnectPort();
            }
        }

        protected virtual void ConnectPort()
        {
            OnConnectPort?.Invoke();
        }
        protected virtual void DisconnectPort()
        {
            OnDisconnectPort?.Invoke();
        }

        public void ClosePort()
        {
            try
            {
                port.DiscardInBuffer();
            }
            catch (Exception ex) { }
            try
            {
                port.DiscardOutBuffer();
            }
            catch (Exception ex) { }
            try
            {
                port.Close();
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                DisconnectPort();
                ArOne.PortIsOpen = port.IsOpen;
            }
            catch (Exception ex) { }
        }
        // read byte array
        protected virtual void ReadByte(byte[] bByte)
        {
            OnReadByte?.Invoke();
        }
        // write byte array
        protected virtual void WriteByte(byte[] bByte)
        {
            OnWriteByte?.Invoke();
        }


        private void ReadExistingComPort()
        {
            string comand = null;
            while (port.IsOpen)
            {
                try
                {
                    //comand = null;
                    //string s = port.ReadExisting();

                    comand = "";
                    string s = port?.ReadExisting();
                    if (s != "" && s.Length != 1)
                    {
                        comand = s.Substring(0, 2);
                        if (comand == "RF")
                        {
                            Int64.TryParse(s.Substring(3, 10), out ArOne.frequency);
                            OnDecodedFrq?.Invoke();
                            Thread.Sleep(40);
                        }

                        switch (comand)
                        {
                            case "MD":
                                int.TryParse(s.Substring(2, 1), out ArOne.Mode);
                                OnDecodedMode?.Invoke();
                                break;
                            case "BW":
                                Int16.TryParse(s.Substring(2, 1), out ArOne.Bandwidth);
                                OnDecodedBW?.Invoke();
                                break;
                            case "HP":
                                int.TryParse(s.Substring(3, 1), out ArOne.HighPassFilter);
                                OnDecodedHPF?.Invoke();
                                break;
                            case "LP":
                                int.TryParse(s.Substring(3, 1), out ArOne.LowPassFilter);
                                OnDecodedLPF?.Invoke();
                                break;
                            case "AC":
                                int.TryParse(s.Substring(2, 1), out ArOne.AGC);
                                OnDecodedAGC?.Invoke();
                                break;
                            case "EN":
                                int.TryParse(s.Substring(2, 1), out ArOne.DEemphasis);
                                OnDecodedByte?.Invoke(comand, ArOne.DEemphasis);
                                break;
                            case "AT":
                                int.TryParse(s.Substring(2, 1), out ArOne.Attenuator);
                                OnDecodedATT?.Invoke();
                                if (s.Length >= 15)
                                {
                                    int.TryParse(s.Substring(15, 3), out ArOne.SignalLevel);
                                    OnDecodedSignalLevel?.Invoke();
                                }
                                //try
                                //{

                                //}
                                //catch (ArgumentOutOfRangeException) when (s.Length > 3) { }                            
                                break;
                            case "AM":
                                int.TryParse(s.Substring(2, 1), out ArOne.Amplifier);
                                OnDecodedAMpl?.Invoke();
                                break;
                            case "BF":
                                int.TryParse(s.Substring(2, 5), out ArOne.BFOFreq);
                                OnDecodedByte?.Invoke(comand, ArOne.BFOFreq);
                                break;
                            case "RQ":
                                int.TryParse(s.Substring(3, 3), out ArOne.NoiseSquelchTreshold);
                                OnDecodedNoiseSquelchTreshold?.Invoke();
                                break;
                            case "DB":
                                int.TryParse(s.Substring(3, 3), out ArOne.LevelSquelchTreshold);
                                OnDecodedLevelSquelchTreshold?.Invoke();
                                break;
                            case "AG":
                                int.TryParse(s.Substring(2, 3), out ArOne.AFGain);
                                OnDecodedAFGain?.Invoke();
                                break;
                            case "MG":
                                int.TryParse(s.Substring(3, 3), out ArOne.ManualGain);
                                OnDecodedManualGain?.Invoke();
                                break;
                            case "RG":
                                int.TryParse(s.Substring(2, 3), out ArOne.ManualGain);
                                OnDecodedRFGain?.Invoke();
                                break;
                            case "IG":
                                int.TryParse(s.Substring(2, 3), out ArOne.IFGain);
                                OnDecodedIFGain?.Invoke();
                                break;
                            case "SQ":
                                int.TryParse(s.Substring(2, 1), out ArOne.SquelchSelect);
                                OnDecodedSquelchSelect?.Invoke();
                                break;
                            case "LS":
                                int.TryParse(s.Substring(2, 3), out ArOne.SignalLevel);
                                OnDecodedSignalLevel?.Invoke();
                                break;
                            case "NS":
                                int.TryParse(s.Substring(7, 3), out ArOne.SignalLevel);
                                OnDecodedSignalLevel?.Invoke();
                                break;
                            case "LC":
                                int.TryParse(s.Substring(2, 1), out ArOne.AutoSignalLevel);
                                OnDecodedAutoSignalLevel?.Invoke();
                                break;
                            case "LA":
                                int.TryParse(s.Substring(2, 1), out ArOne.AutoBackLit);
                                OnDecodedByte?.Invoke(comand, ArOne.AutoBackLit);
                                break;
                            case "BL":
                                int.TryParse(s.Substring(2, 1), out ArOne.BackLit_OnOff);
                                OnDecodedByte?.Invoke(comand, ArOne.BackLit_OnOff);
                                break;
                            case "LD":
                                int.TryParse(s.Substring(2, 1), out ArOne.BackLitDimmer);
                                OnDecodedByte?.Invoke(comand, ArOne.BackLitDimmer);
                                break;
                            case "LV":
                                int.TryParse(s.Substring(2, 2), out ArOne.LCDContrast);
                                OnDecodedByte?.Invoke(comand, ArOne.LCDContrast);
                                break;
                            case "BV":
                                int.TryParse(s.Substring(2, 1), out ArOne.BeepLevel);
                                OnDecodedByte?.Invoke(comand, ArOne.BeepLevel);
                                break;
                            case "SO":
                                int.TryParse(s.Substring(2, 1), out ArOne.SpeakerSelect);
                                OnDecodedByte?.Invoke(comand, ArOne.SpeakerSelect);
                                break;
                            case "PO":
                                int.TryParse(s.Substring(2, 1), out ArOne.ExternalSpeacker);
                                OnDecodedByte?.Invoke(comand, ArOne.ExternalSpeacker);
                                break;
                            case "DD":
                                if (s.Length == 7) { Arone.DelayTime = s.Substring(2, 3); }
                                else { Arone.DelayTime = s.Substring(2, 2); }
                                OnDelayScan?.Invoke();
                                Thread.Sleep(50);
                                break;
                            case "SP":
                                //double.TryParse(s.Substring(2, 3), out Arone.FreeScan);
                                string s1 = s.Substring(2, 1);
                                string s2 = s.Substring(4, 1);
                                string res = s1 + "," + s2;
                                double.TryParse(res, out Arone.FreeScan);
                                OnFreeScan?.Invoke();
                                break;
                            case "SF":
                                int.TryParse(s.Substring(2, 1), out ArOne.SignalMeterDisplay);
                                OnDecodedByte?.Invoke(comand, ArOne.SignalMeterDisplay);
                                break;
                            case "OF":
                                int.TryParse(s.Substring(2, 3), out ArOne.DuplexMode);
                                OnDecodedByte?.Invoke(comand, ArOne.DuplexMode);
                                break;
                            case "OL":
                                int DplFrqN;//запрос: DplFrqN могло быть 0..47, а тут 1..19 ???????????
                                int.TryParse(s.Substring(2, 2), out DplFrqN);
                                int.TryParse(s.Substring(5, 10), out ArOne.DuplexFrequency[DplFrqN]);
                                OnDecodedByte?.Invoke(comand, ArOne.DuplexFrequency[DplFrqN]);
                                break;
                            case "LU":
                                int.TryParse(s.Substring(2, 3), out ArOne.SignalLevelUnit_dBmV);
                                OnDecodedSignalLevelUnit_dBmV?.Invoke();
                                break;
                            case "LB":
                                try
                                {
                                    int.TryParse(s.Substring(3, 4), out ArOne.SignalLevelUnit_dBm);
                                    OnDecodedSignalLevelUnit_dBm?.Invoke();
                                }
                                catch (ArgumentOutOfRangeException) when (s.Length == 7) { }
                                //int.TryParse(s.Substring(4, 3), out ArOne.SignalLevelUnit_dBm);
                                //OnDecodedSignalLevelUnit_dBm?.Invoke();
                                break;
                            //case "SR": 
                            //int.TryParse(s.Substring(2, 1), out ArOne.Mode); 
                            //if (OnDecodedByte != null)
                            //{
                            //    OnDecodedByte(comand, ArOne.Mode);/
                            //}
                            //break;
                            //case "PR": 
                            //int.TryParse(s.Substring(5, 10), out ArOne.PassFrequencyList[int.Parse(s.Substring(2, 2))]); 
                            //if (OnDecodedByte != null)
                            //{
                            //    OnDecodedByte(comand, );
                            //}
                            //break;
                            case "GA":
                                int.TryParse(s.Substring(2, 1), out ArOne.SelectMemory_OnOff);
                                OnDecodedByte?.Invoke(comand, ArOne.SelectMemory_OnOff);
                                break;
                            case "GR":
                                int.TryParse(s.Substring(2, 2), out ArOne.SelectMemoryList);
                                OnDecodedByte?.Invoke(comand, ArOne.SelectMemoryList);
                                break;
                            case "MR":
                                //Arone.MemoryData = s.Substring(0, 61);
                                break;
                            case "MX":
                                //int.TryParse(s.Substring(2, 4), out Arone.MemoryData);
                                Arone.MemoryData = s;
                                Arone.memory = new List<string>(s.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries));

                                foreach (var item in Arone.memory)
                                {
                                    Arone.memTwo.Add(item);
                                }
                                IEnumerable<string> mem = Arone.memTwo.Distinct();
                                Arone.memthree = mem.ToList();
                                //Arone.dataChannel.Add(Arone.memthree);
                                OnDecodedMemoryChannelDataRead?.Invoke();
                                Thread.Sleep(50);
                                break;
                            case "MS":
                                Arone.ModeScan = s.Substring(5, 3);
                                OnDecodedScanMode?.Invoke();
                                break;
                        }
                    }
                    Thread.Sleep(40);
                }

                catch (Exception ex)
                {
                    OnDecodedByte?.Invoke("Err " + comand, -1);
                }

            }
        }

    }
}